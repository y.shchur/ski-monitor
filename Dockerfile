FROM openjdk:11.0.7-jre

LABEL maintainer="yurii.szczur@gmail.com"

RUN mkdir /app

COPY ./ski-monitor-server/target/ski-monitor-server-0.0.1-SNAPSHOT.jar /app

WORKDIR /app

EXPOSE 8080

ENTRYPOINT [ "java", "-jar", "./ski-monitor-server-0.0.1-SNAPSHOT.jar" ]

