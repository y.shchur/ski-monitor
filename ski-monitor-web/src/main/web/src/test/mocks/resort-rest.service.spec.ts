export class ResortRestMockService {
  
  findMatching = jasmine.createSpy('findMatching');

  findForecastByResortId = jasmine.createSpy('findForecastByResortId');
}