import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResortSearchComponent } from './resort/resort-search/resort-search.component';
import { ResortDetailComponent } from './resort/resort-detail/resort-detail.component';


const routes: Routes = [
  { path: '', redirectTo: '/resort', pathMatch: 'full' },
  { path: 'resort', component: ResortSearchComponent },
  { path: 'resort/:id', component: ResortDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
