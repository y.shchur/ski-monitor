import { CommonModule } from "@angular/common";
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbCollapseModule, NgbDropdownModule, NgbModalModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DialogComponent } from './dialog/dialog.component';
import { NavBarComponent } from "./nav-bar/nav-bar/nav-bar.component";
import { SideNavBarComponent } from "./nav-bar/side-nav-bar/side-nav-bar.component";
import { TempPipe } from './pipe/temp.pipe';
import { LocalStorageService } from './storage/local-storage.service';
import { TableComponent } from './table/table.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    NavBarComponent,
    SideNavBarComponent,
    TableComponent,
    DialogComponent,
    TempPipe
  ],
  exports: [
    NavBarComponent,
    SideNavBarComponent,
    TableComponent,
    DialogComponent,
    TranslateModule,
    TempPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    NgbNavModule,
    NgbCollapseModule,
    NgbDropdownModule,
    NgbModalModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      }
    })
  ],
  providers: [
    LocalStorageService
  ]
})
export class CoreModule { }
