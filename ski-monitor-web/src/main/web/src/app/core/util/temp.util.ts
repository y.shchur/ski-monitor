export function toFahrengeit(celsius: number) {
  return celsius * 1.8 + 32;
}

export function toDegreeString(value: number) {
  const formatted = value > 0 ? '+' + value : '' + value;
  return formatted + '\xB0';
}