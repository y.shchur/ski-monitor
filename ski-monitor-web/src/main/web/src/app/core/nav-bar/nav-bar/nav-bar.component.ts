import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'sm-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  langs: string[];
  currentLang: string;

  isMenuCollapsed = true;

  constructor(private translateService: TranslateService) {
    this.langs = translateService.langs;
    this.currentLang = translateService.currentLang;
  }

  ngOnInit() {
  }

  changeLang(lang: string) {
    this.translateService.use(lang);
    localStorage.setItem('preferredLang', lang);
  }

}
