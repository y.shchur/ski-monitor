import { Component, OnInit, ViewChild } from "@angular/core";
import { MatDrawer, MatSidenav } from "@angular/material";

@Component({
  selector: "sm-side-nav-bar",
  templateUrl: "./side-nav-bar.component.html",
  styleUrls: ["./side-nav-bar.component.scss"]
})
export class SideNavBarComponent implements OnInit {

  @ViewChild(MatSidenav, {static: false})
  drawer: MatSidenav;

  constructor() {}

  ngOnInit() {}

  toggle() {
    this.drawer.toggle();
  }
}
