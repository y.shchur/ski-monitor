import { Pipe, PipeTransform } from '@angular/core';
import * as round from 'lodash/round'
import { toDegreeString, toFahrengeit } from '../util/temp.util';

@Pipe({
  name: 'temp'
})
export class TempPipe implements PipeTransform {

/**
 * Transform to temperature string
 * @param value temperature in Celsius
 * @param args 
 */
  transform(value: unknown, ...args: unknown[]): unknown {
    const rounded = round(value, 0);
    switch (args[0]) {
      case 'C':
        return toDegreeString(rounded) + 'C';
      case 'F':
        return toDegreeString(toFahrengeit(rounded)) + 'F'
      default:
        return toDegreeString(rounded);
    }
  }

}
