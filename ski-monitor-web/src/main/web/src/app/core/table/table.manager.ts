import { Observable, Subscription } from "rxjs";
import { TableData, ColumnDef, TableComponent } from "./table.component";
import { tap } from 'rxjs/operators';
import { TableEventManager } from './table-event.manager';

export class TableManager<T extends TableData> {

  rows: T[] | Observable<T[]>;

  columnNames: string[];

  eventManager: TableEventManager<T> = new TableEventManager();

  private dataSourceSubscription: Subscription;
  private uuidv4 = require("uuid/v4");

  constructor(
    public columnDefs: ColumnDef[],
    private dataSource: T[] | Observable<T[]>
  ) {  }

  init(tableComponent: TableComponent<T>) {
    this.columnNames = this.columnDefs.map(def => def.name || def.field);
    this.setRows();
    
  }

  destroy(){
    this.dataSourceSubscription && this.dataSourceSubscription.unsubscribe();
    this.eventManager.destroy();
    console.log('destroyed');
  }

  private setRows() {
    if (this.dataSource instanceof Array) {
      this.enrichDataWithUuid(this.dataSource);
      this.rows = this.dataSource;
    }
    else {
      this.rows = this.dataSource.pipe(tap(data => this.enrichDataWithUuid(data)));
      this.dataSourceSubscription = this.dataSource.subscribe();
    }
  }

  private enrichDataWithUuid(data: T[]): void {
    data.forEach(element => (element.uuid = element.uuid || this.uuidv4()));
  }
}
