import { Subject } from 'rxjs';

export class TableEventManager<T> {
  onRowClick$: Subject<T> = new Subject();

  destroyed$: Subject<boolean> = new Subject();

  constructor() {}

  destroy() {
    this.onRowClick$.complete();
    this.destroyed$.next(true);
  }
}
