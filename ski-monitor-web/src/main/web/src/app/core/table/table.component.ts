import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { TableEventManager } from './table-event.manager';
import { TableManager } from './table.manager';

@Component({
  selector: 'sm-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent<T extends TableData> implements OnInit, OnDestroy {
  @Input()
  tableManager: TableManager<T>;

  eventManager: TableEventManager<T>;

  ngOnInit() {
    if (!this.tableManager) {
      throw 'Table Manager is not provided';
    }
    this.tableManager.init(this);
    this.eventManager = this.tableManager.eventManager;
  }

  ngOnDestroy(): void {
    this.tableManager.destroy();
  }

  onRowClick(element: T) {
    this.eventManager.onRowClick$.next(element);
  }
}

export interface TableData {
  uuid?: string;
}

export interface ColumnDef {
  field: string;
  name: string;
  header: string;
}
