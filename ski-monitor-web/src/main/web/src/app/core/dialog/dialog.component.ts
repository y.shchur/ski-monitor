import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'sm-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit, OnDestroy {

  @Input()
  hasHeader: boolean;

  @Input()
  title: string;

  constructor(
    private modalService: NgbModal
  ) { }

  closeDialog() {
    this.modalService.dismissAll("close");
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {

  }
}
