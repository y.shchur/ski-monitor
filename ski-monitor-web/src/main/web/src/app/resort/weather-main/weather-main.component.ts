import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { WeatherEto } from '../domain/weather.eto';

@Component({
  selector: 'sm-weather-main',
  templateUrl: './weather-main.component.html',
  styleUrls: ['./weather-main.component.scss']
})
export class WeatherMainComponent implements OnInit {

  @Input()
  weather: WeatherEto;

  @Input()
  vertical = true;

  @Input()
  size: 'S' | 'M' | 'L' = 'L';

  owfSize = {
    'S': 'owf-1x',
    'M': 'owf-2x',
    'L': 'owf-4x'
  }

  constructor() { }

  ngOnInit(): void {
  }

  getIconStyles(): string[] {
    const currentHour = moment().hours();
    const dayTimeSuffix = currentHour > 18 || currentHour < 6 ? 'n' : 'd';
    return ['owf', `owf-${this.weather.code}-${dayTimeSuffix}`, this.owfSize[this.size]];
  }
}
