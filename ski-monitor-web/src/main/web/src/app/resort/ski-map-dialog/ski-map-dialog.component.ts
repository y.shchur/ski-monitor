import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'sm-ski-map-dialog',
  templateUrl: './ski-map-dialog.component.html',
  styleUrls: ['./ski-map-dialog.component.scss']
})
export class SkiMapDialogComponent implements OnInit {

  currentZoomStyle = new BehaviorSubject(this.getScale(1));

  data: any;

  private currentZoom = 1;

  private minZoom = 1;

  constructor() { }

  ngOnInit() {
    console.log(this.data);
  }
  zoom(param: number) {
    const newZoom = this.currentZoom + param;
    if (newZoom >= this.minZoom) {
      this.currentZoom = newZoom;
      this.currentZoomStyle.next(this.getScale(this.currentZoom));
    }
  }
  getScale(param: number) {
    return `scale(${param})`;
  }

}
