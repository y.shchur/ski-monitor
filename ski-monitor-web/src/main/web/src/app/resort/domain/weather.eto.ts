export interface WeatherEto {
  resortId: number;
  timestamp: string;
  temp: number;
  tempMin: number;
  tempMax: number;
  pressure: number;
  seaLevel: number;
  grndLevel: number;
  humidity: number;
  rainVol1h: number;
  rainVol3h: number;
  snowVol1h: number;
  snowVol3h: number;
  windSpeed: number;
  windDirection: number;
  cloudiness: number;
  code: number;
}