export interface ResortEto {
  
  id: number

  name: string

  skiRunTotalLength: number;
}