import { TableData } from "src/app/core/table/table.component";

export class ResortResponseTo implements TableData {
  uuid?: string;
  id: number;
  name: string;
}
