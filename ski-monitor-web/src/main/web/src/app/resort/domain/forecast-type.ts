export class ForecastType {
  
  private static ALL_VALUES = new Map<string, ForecastType>();

  static readonly DAILY_FORECAST = new ForecastType('DAILY_FORECAST', 'dailyForecast');

  static readonly _5_DAY_FORECAST = new ForecastType('_5_DAY_FORECAST', '5DayForecast');

  private constructor(public value, public typeName: string) {
    ForecastType.ALL_VALUES.set(value, this);
  }

  static of(value: string): ForecastType {
    return ForecastType.ALL_VALUES.get(value);
  }
}