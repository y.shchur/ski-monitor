export interface ResortWithForecastSearchTo {
  resortIds: number[];
  forecastType: string;
}