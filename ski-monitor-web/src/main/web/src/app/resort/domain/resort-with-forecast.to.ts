import { ResortEto } from './resort.eto';
import { WeatherEto } from './weather.eto';

export interface ResortWithForecastTo {

  resort: ResortEto;

  forecast: WeatherEto[];

}