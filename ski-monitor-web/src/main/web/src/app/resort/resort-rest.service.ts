import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { ResortResponseTo } from "./domain/resort-response.to";
import { ResortWithForecastTo } from './domain/resort-with-forecast.to';
import { ForecastType } from './domain/forecast-type';
import { ResortWithForecastSearchTo } from './domain/resort-with-forecast-search.to';

@Injectable()
export class ResortRestService {
  protected httpOptions = {
    headers: new HttpHeaders({
      "content-type": "application/json"
    })
  };

  constructor(protected http: HttpClient) { }

  findMatching(name: string): Observable<ResortResponseTo[]> {
    // return of([{ name: 'Szklarska' }, { name: 'Karpacz' }, { name: 'Harrachov' }]);

    return this.http
      .get<ResortResponseTo[]>(`api/resort/findMatch/${name}`, this.httpOptions)
      .pipe(catchError(error => of([])));
  }

  findForecastByResortId(id: number, forecastType: ForecastType): Observable<ResortWithForecastTo> {
    const options = {
      ...this.httpOptions,
      params: new HttpParams().append('id', '' + id).append('forecastType', forecastType.value)
    };
    return this.http
      .get<ResortWithForecastTo>(`api/resort/findForecastByResortId`, options)
      .pipe(catchError(error => of({} as ResortWithForecastTo)));
  }

  findForecastsByCriteria(searchCriteria: ResortWithForecastSearchTo): Observable<ResortWithForecastTo[]> {
    const options = {
      ...this.httpOptions,
      params: new HttpParams()
        .append('resortIds', searchCriteria.resortIds.join(','))
        .append('forecastType', searchCriteria.forecastType)
    };
    return this.http
      .get<ResortWithForecastTo[]>(
        `api/resort/findForecastsByResortIdsAndType`, options)
      .pipe(catchError(error => of(null)));
  }
}
