import { AbstractForecastChartCreator } from './abstract-forecast-chart-creator';
import { WeatherEto } from '../domain/weather.eto';
import * as Chartist from 'chartist';
import * as moment from 'moment';

export class Forecast24HourChartCreator extends AbstractForecastChartCreator {


  initializeChart(forecast: WeatherEto[], containerClass: string) {
    var chart = new Chartist.Line(containerClass, {
      series: [
        {
          data: this.extractValues(forecast)
        }
      ]
    },
      {
        ...this.defaultChartOptions,
        axisX: {
          type: Chartist.FixedScaleAxis,
          divisor: forecast.length - 1,
          labelInterpolationFnc: (value) => moment(value).format('HH:mm')
        }
      });
  }


}