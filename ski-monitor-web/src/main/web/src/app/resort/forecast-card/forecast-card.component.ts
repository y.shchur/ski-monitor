import { Component, OnInit, Input } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { WeatherEto } from '../domain/weather.eto';
import * as moment from 'moment';
import { Line } from 'chartist';
import { ForecastType } from '../domain/forecast-type';
import { ResortForecastService } from '../resort-forecast.service';
import { tap } from 'rxjs/operators';
import * as Chartist from 'chartist';
import { ForecastChartCreatorFactory } from './forecast-chart-creator.service';

@Component({
  selector: 'sm-forecast-card',
  templateUrl: './forecast-card.component.html',
  styleUrls: ['./forecast-card.component.scss']
})
export class ForecastCardComponent implements OnInit {

  @Input()
  forecastService: ResortForecastService;

  @Input()
  resortId: number;

  activeChartType$ = new BehaviorSubject(ForecastType.DAILY_FORECAST.value);

  constructor() { }

  ngOnInit() {
    this.activeChartType$.subscribe(
      activeChartType =>
        this.forecastService.getForecast(this.resortId, ForecastType.of(activeChartType)).then(forecast => {
          ForecastChartCreatorFactory.getChartCreator(ForecastType.of(activeChartType))
            .initializeChart(forecast, '.ct-chart')
        }));
  }

  openTab(tabName: string){
    this.activeChartType$.next(tabName)
    return false;
  }

}
