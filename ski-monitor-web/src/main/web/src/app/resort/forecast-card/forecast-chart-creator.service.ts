import { Injectable } from '@angular/core';
import { ForecastType } from '../domain/forecast-type';
import * as Chartist from 'chartist';
import { Forecast24HourChartCreator } from './forecast-24hour-chart-creator';
import { Forecast5DayChartCreator } from './forecast-5day-chart-creator';
import { AbstractForecastChartCreator } from './abstract-forecast-chart-creator';


export class ForecastChartCreatorFactory {

  private static readonly chartCreators = new Map<ForecastType, AbstractForecastChartCreator>([
    [ForecastType.DAILY_FORECAST, new Forecast24HourChartCreator()],
    [ForecastType._5_DAY_FORECAST, new Forecast5DayChartCreator()]
  ]);

  private constructor() { }

  static getChartCreator(forecastType) {
    const creator = ForecastChartCreatorFactory.chartCreators.get(forecastType);
    if (!creator) {
      throw new Error('No chart creator for forecast type: ' + forecastType.value)
    }
    return creator;
  }
}
