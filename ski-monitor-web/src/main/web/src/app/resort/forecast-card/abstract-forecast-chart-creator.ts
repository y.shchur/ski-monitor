import { WeatherEto } from '../domain/weather.eto';
import { ILineChartOptions } from 'chartist';

export abstract class AbstractForecastChartCreator {

  protected readonly defaultChartOptions = {
    showArea: true,
    showPoint: false,
    axisY: { onlyInteger: true, referenceValue: 0 }
  } as ILineChartOptions;

  abstract initializeChart(forecast: WeatherEto[], containerClass: string);

  protected extractValues(forecast: WeatherEto[]) {
    const values = forecast.map(item => this.mapToXY(item));
    console.log(values);
    return values;
  }

  protected mapToXY(item: WeatherEto) {
    return { x: new Date(item.timestamp), y: item.temp }
  }
}