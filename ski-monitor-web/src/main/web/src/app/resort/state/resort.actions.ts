import { Action } from '@ngrx/store';
import { ResortEto } from '../domain/resort.eto';
import { Forecast24HourChartCreator } from '../forecast-card/forecast-24hour-chart-creator';
import { ForecastType } from '../domain/forecast-type';
import { ResortWithForecastTo } from '../domain/resort-with-forecast.to';
import { ResortWithForecastSearchTo } from '../domain/resort-with-forecast-search.to';

export enum ResortActionType {
  PIN_RESORT = '[Resort] pin resort',
  UNPIN_RESORT = '[Resort] unpin resort',
  FETCH_PINNED_RESORTS_FORECAST = '[Resort] fetch pinned resort with forecast',
  FETCH_PINNED_RESORTS_FORECAST_SUCCESS = '[Resort] fetch pinned resort with forecast success'
}

export class PinResort implements Action {
  readonly type = ResortActionType.PIN_RESORT;

  /**
   * Constructor
   * @param payload resortId
   */
  constructor(public payload: number) {
  }
}


export class UnpinResort implements Action {
  readonly type = ResortActionType.UNPIN_RESORT;

  /**
   * Constructor
   * @param payload resortId
   */
  constructor(public payload: number) {
  }
}

export class FetchPinnedResortForecast implements Action {
  readonly type = ResortActionType.FETCH_PINNED_RESORTS_FORECAST;

  constructor(public payload: ForecastType) {

  }
}

export class FetchPinnedResortForecastSuccess implements Action {
  readonly type = ResortActionType.FETCH_PINNED_RESORTS_FORECAST_SUCCESS;

  constructor(public payload: ResortWithForecastTo[]) {

  }
}


export type ResortAction =
  PinResort
  | UnpinResort
  | FetchPinnedResortForecast
  | FetchPinnedResortForecastSuccess;