
import {InjectionToken} from '@angular/core';
import { ResortState } from './resort.state';
import { StoreConfig } from '@ngrx/store';
import { ResortAction } from './resort.actions';

// token for the state keys.
export const RESORT_STORAGE_KEYS = new InjectionToken<keyof ResortState>('ResortStoreKeys');
// token for the localStorage key.
export const RESORT_LOCAL_STORAGE_KEY = new InjectionToken<string[]>('resortStorage');

export const RESORT_CONFIG_TOKEN = 
 new InjectionToken<StoreConfig<ResortState, ResortAction>>('CoursesConfigToken');
  