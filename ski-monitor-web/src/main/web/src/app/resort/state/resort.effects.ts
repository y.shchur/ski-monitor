import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { filter, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { AppState } from 'src/app/state/app.state';
import { ResortWithForecastSearchTo } from '../domain/resort-with-forecast-search.to';
import { ResortRestService } from '../resort-rest.service';
import { FetchPinnedResortForecast, FetchPinnedResortForecastSuccess, ResortActionType } from './resort.actions';
import { selectPinnedResortIds } from './resort.selectors';

@Injectable()
export class ResortEffects {

  @Effect()
  fetchResortsWithForecast$ = this.actions$.pipe(
    ofType<FetchPinnedResortForecast>(ResortActionType.FETCH_PINNED_RESORTS_FORECAST),
    withLatestFrom(this.store.select(selectPinnedResortIds)),
    filter(([, resortIds]) => resortIds && resortIds.length > 0),
    map(([action, resortIds]) =>
      ({ resortIds: [...resortIds], forecastType: action.payload.value } as ResortWithForecastSearchTo)),
    switchMap(searchTo => this.resortRestService.findForecastsByCriteria(searchTo)),
    filter(resortsWithForecasts => !!resortsWithForecasts),
    switchMap(resortsWithForecasts => of(new FetchPinnedResortForecastSuccess(resortsWithForecasts)))
  );


  constructor(
    private store: Store<AppState>,
    private actions$: Actions,
    private resortRestService: ResortRestService) { }

}