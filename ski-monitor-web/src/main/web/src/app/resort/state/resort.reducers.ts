import { initialResortState, ResortState } from './resort.state';
import { ResortAction, ResortActionType } from './resort.actions';
import { filter } from 'rxjs/operators';

export const resortReducers = (state = initialResortState, action: ResortAction): ResortState => {
  switch (action.type) {
    case ResortActionType.PIN_RESORT:
      return {
        ...state,
        pinnedResortIds: [...state.pinnedResortIds, action.payload]
      }
    case ResortActionType.UNPIN_RESORT:
      const resortIds = state.pinnedResortIds.filter(id => id != action.payload);
      const filteredResorts = state.pinnedResortsWithForecasts
        .filter(resortWithForecast => resortWithForecast.resort.id !== action.payload);
      return {
        ...state,
        pinnedResortIds: resortIds,
        pinnedResortsWithForecasts: filteredResorts
      }
    case ResortActionType.FETCH_PINNED_RESORTS_FORECAST_SUCCESS:
      return {
        ...state,
        pinnedResortsWithForecasts: action.payload
      }
    default:
      return state;
  }
}