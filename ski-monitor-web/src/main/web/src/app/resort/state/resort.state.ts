import { ResortEto } from '../domain/resort.eto';
import { ResortWithForecastTo } from '../domain/resort-with-forecast.to';

export interface ResortState {
  pinnedResortIds: number[];
  pinnedResortsWithForecasts: ResortWithForecastTo[];
}

export const initialResortState = {
  pinnedResortIds: [],
  pinnedResortsWithForecasts: []
} as ResortState;