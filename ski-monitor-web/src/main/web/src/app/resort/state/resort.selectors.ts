import { AppState } from 'src/app/state/app.state';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ResortState } from './resort.state';

const selectResortState = createFeatureSelector('Resort');

export const selectPinnedResortIds = createSelector(
  selectResortState,
  (state: ResortState) => state.pinnedResortIds
)

export const selectPinnedResortsWithForecast = createSelector(
  selectResortState,
  (state: ResortState) => state.pinnedResortsWithForecasts
)