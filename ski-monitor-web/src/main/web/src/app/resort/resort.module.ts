import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ChartistModule } from 'ng-chartist';
import { DragScrollModule } from 'ngx-drag-scroll';
import { CoreModule } from '../core/core.module';
import { ForecastCardComponent } from './forecast-card/forecast-card.component';
import { ResortDetailComponent } from './resort-detail/resort-detail.component';
import { ResortPinnedComponent } from './resort-pinned/resort-pinned.component';
import { ResortRestService } from './resort-rest.service';
import { ResortSearchComponent } from './resort-search/resort-search.component';
import { SkiMapDialogComponent } from './ski-map-dialog/ski-map-dialog.component';
import { ResortEffects } from './state/resort.effects';
import { resortReducers } from './state/resort.reducers';
import { RESORT_LOCAL_STORAGE_KEY, RESORT_STORAGE_KEYS, RESORT_CONFIG_TOKEN } from './state/resort.tockens';
import { WeatherCardComponent } from './weather-card/weather-card.component';
import { WeatherMainComponent } from './weather-main/weather-main.component';
import { LocalStorageService } from '../core/storage/local-storage.service';
import { storageMetaReducerFactory } from '../core/storage/storage-meta.reducer';

@NgModule({
  declarations: [
    ResortSearchComponent,
    ResortDetailComponent,
    WeatherCardComponent,
    ForecastCardComponent,
    SkiMapDialogComponent,
    ResortPinnedComponent,
    WeatherMainComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    ChartistModule,
    DragScrollModule,
    NgbTypeaheadModule,
    StoreModule.forFeature('Resort', resortReducers, RESORT_CONFIG_TOKEN),
    EffectsModule.forFeature([ResortEffects])
  ],
  providers: [
    { provide: RESORT_LOCAL_STORAGE_KEY, useValue: '__resort_storage__' },
    { provide: RESORT_STORAGE_KEYS, useValue: ['pinnedResortIds'] },
    {
      provide: RESORT_CONFIG_TOKEN,
      deps: [RESORT_STORAGE_KEYS, RESORT_LOCAL_STORAGE_KEY, LocalStorageService],
      useFactory: storageMetaReducerFactory
    },
    ResortRestService
  ],
  entryComponents: [
    SkiMapDialogComponent
  ]
})
export class ResortModule { }
