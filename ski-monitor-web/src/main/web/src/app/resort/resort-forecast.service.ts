import { Injectable } from '@angular/core';
import { ResortRestService } from './resort-rest.service';
import { ResortEto } from './domain/resort.eto';
import { WeatherEto } from './domain/weather.eto';
import { Observable } from 'rxjs';
import { ForecastType } from './domain/forecast-type';
import { tap, map } from 'rxjs/operators';

@Injectable()
export class ResortForecastService {

  private resorts = new Map<number, Promise<ResortEto>>();

  private forecastsForResorts = new Map<number, Map<string, Promise<WeatherEto[]>>>();

  constructor(private resortRestService: ResortRestService) { }

  getResort(resortId: number): Promise<ResortEto> {
    if (resortId && this.resorts.has(resortId)) {
      return this.resorts.get(resortId);
    }
    this.getForecast(resortId, ForecastType.DAILY_FORECAST);
    return this.resorts.get(resortId);
  }

  getForecast(resortId: number, forecastType: ForecastType): Promise<WeatherEto[]> {
    const resortForecasts = this.forecastsForResorts.get(resortId);

    if (resortForecasts && resortForecasts.has(forecastType.value)) {
      return resortForecasts.get(forecastType.value);
    }

    const resortWithForecast = this.resortRestService.findForecastByResortId(resortId, forecastType);

    this.resorts.set(resortId, resortWithForecast.pipe(map(response => response.resort)).toPromise());
    const forecast = resortWithForecast.pipe(map(response => response.forecast)).toPromise();

    if (resortForecasts) {
      resortForecasts.set(forecastType.value, forecast);
    } else {
      this.forecastsForResorts.set(resortId, new Map([[forecastType.value, forecast]]));
    }
    return forecast;
  }
}

