import { Component, OnInit, Input } from '@angular/core';
import { WeatherEto } from '../domain/weather.eto';
import * as moment from 'moment';
import * as _ from 'lodash';
import { ResortEto } from '../domain/resort.eto';
import { AppState } from 'src/app/state/app.state';
import { Store } from '@ngrx/store';
import { PinResort } from '../state/resort.actions';
import { selectPinnedResortIds } from '../state/resort.selectors';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ResortState } from '../state/resort.state';

@Component({
  selector: 'sm-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})
export class WeatherCardComponent implements OnInit {

  @Input()
  resort: ResortEto;

  @Input()
  weather: WeatherEto;

  isPinned$: Observable<boolean>;

  constructor(private store: Store<ResortState>) {
  }

  ngOnInit() {
    this.isPinned$ = this.store.select(selectPinnedResortIds).pipe(
      map(resortIds => resortIds.some(id => id === this.resort.id)));
  }

  pinResort() {
    this.store.dispatch(new PinResort(this.resort.id));
  }

}
