import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app.state';
import { selectPinnedResortIds, selectPinnedResortsWithForecast } from '../state/resort.selectors';
import { FetchPinnedResortForecast, UnpinResort } from '../state/resort.actions';
import { withLatestFrom } from 'rxjs/operators';
import { ForecastType } from '../domain/forecast-type';
import { Router } from '@angular/router';
import { ResortWithForecastTo } from '../domain/resort-with-forecast.to';

@Component({
  selector: 'sm-resort-pinned',
  templateUrl: './resort-pinned.component.html',
  styleUrls: ['./resort-pinned.component.scss']
})
export class ResortPinnedComponent implements OnInit {

  pinnedResorts$ = this.store.select(selectPinnedResortsWithForecast);

  constructor(
    private store: Store<AppState>,
    private router: Router
  ) {
    store.dispatch(new FetchPinnedResortForecast(ForecastType.DAILY_FORECAST))
  }

  ngOnInit(): void {
  }

  getCurrentWeather(resortWithForecast: ResortWithForecastTo) {
    return resortWithForecast.forecast[0];
  }

  unpinResort(resortId: number): void {
    this.store.dispatch(new UnpinResort(resortId));
  }

  navigateToResortDetails(resortId) {
    console.log(resortId);
    this.router.navigateByUrl(`/resort/${resortId}`);
    return false;
  }
}
