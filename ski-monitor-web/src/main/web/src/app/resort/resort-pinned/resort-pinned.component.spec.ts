import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResortPinnedComponent } from './resort-pinned.component';

describe('ResortPinnedComponent', () => {
  let component: ResortPinnedComponent;
  let fixture: ComponentFixture<ResortPinnedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResortPinnedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResortPinnedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
