import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';
import { ForecastType } from '../domain/forecast-type';
import { ResortEto } from '../domain/resort.eto';
import { WeatherEto } from '../domain/weather.eto';
import { ResortForecastService } from '../resort-forecast.service';

@Component({
  selector: 'sm-resort-detail',
  templateUrl: './resort-detail.component.html',
  styleUrls: ['./resort-detail.component.scss'],
  providers: [ResortForecastService]
})
export class ResortDetailComponent implements OnInit {

  @ViewChild('modalContent')
  modalContent: TemplateRef<any>;

  resortId: number;

  currentWeather: WeatherEto;

  resort$: Promise<ResortEto>;

  skiMapDialogOpenned = new BehaviorSubject(false);

  constructor(
    public forecastService: ResortForecastService,
    private route: ActivatedRoute,
    private modalService: NgbModal) {
  }

  ngOnInit() {
    this.resortId = +this.route.snapshot.params.id;
    this.resort$ = this.forecastService.getResort(this.resortId);
    this.forecastService.getForecast(this.resortId, ForecastType.DAILY_FORECAST)
      .then(
        forecast => {
          this.currentWeather = forecast[0];
        });
  }

  openSkiMapDialog() {
    this.modalService.open(this.modalContent, {size: 'xl', centered: true});
  }
}
