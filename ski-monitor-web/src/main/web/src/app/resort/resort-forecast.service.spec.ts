import { TestBed } from '@angular/core/testing';

import { ResortForecastService } from './resort-forecast.service';
import { ResortRestService } from './resort-rest.service';
import { ResortRestMockService } from '../../test/mocks/resort-rest.service.spec';
import { ForecastType } from './domain/forecast-type';
import { of, Subject } from 'rxjs';
import { ResortWithForecastTo } from './domain/resort-with-forecast.to';

describe('ForecastService', () => {
  let forecastService: ResortForecastService;
  let resortRestService: ResortRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ResortForecastService,
        { provide: ResortRestService, useClass: ResortRestMockService }
      ]
    });
    forecastService = TestBed.get(ResortForecastService);
    resortRestService = TestBed.get(ResortRestService);
  });

  fit('should fetch resort and forecast once', (done) => {
    // given
    const resortId = 1;
    const response = { resort: { id: resortId }, forecast: [{ temp: 10 }] } as ResortWithForecastTo
    const server$ = new Subject();
    resortRestService.findForecastByResortId = jasmine.createSpy('findForecastByResortId')
      .and.returnValue(server$.asObservable());

    // when
    const resort = forecastService.getResort(resortId);
    const forecast = forecastService.getForecast(resortId, ForecastType.DAILY_FORECAST);
    Promise.all([resort, forecast]).then((values) => {
      // async then
      console.log(values)
      expect(resort).toEqual(response.resort);
      expect(forecast).toEqual(response.forecast);
      expect(resortRestService.findForecastByResortId).toHaveBeenCalledWith(resortId, ForecastType.DAILY_FORECAST);
      expect(resortRestService.findForecastByResortId).toHaveBeenCalledTimes(1);
      done();
    });

    server$.next(response);
    server$.complete();
  });
});
