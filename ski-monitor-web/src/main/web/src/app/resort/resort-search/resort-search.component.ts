import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of, Subject, combineLatest, BehaviorSubject, merge } from 'rxjs';
import { debounceTime, filter, switchMap, takeUntil, tap, map } from 'rxjs/operators';
import { ResortRestService } from '../resort-rest.service';
import { ResortResponseTo } from '../domain/resort-response.to';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { AppState } from 'src/app/state/app.state';
import { Store, select } from '@ngrx/store';
import { selectPinnedResortIds } from '../state/resort.selectors';

@Component({
  selector: 'sm-resort-search',
  templateUrl: './resort-search.component.html',
  styleUrls: ['./resort-search.component.scss']
})
export class ResortSearchComponent implements OnInit, OnDestroy {
  model: any;

  foundResorts = [];

  destroyed$: Subject<boolean> = new Subject();

  constructor(
    private resortRestService: ResortRestService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  formatter = (resort) => resort.name;

  getResortSearchResult = (keyword$: Observable<string>): Observable<ResortResponseTo[]> => {
    const serverSearch = keyword$.pipe(
      filter(keyword => keyword.length > 2),
      debounceTime(1500),
      switchMap(keyword => this.resortRestService.findMatching(keyword)),
      tap(resorts => this.foundResorts = resorts));
    const filterFetched = keyword$.pipe(
      map(keyword => this.filterResorts(keyword, this.foundResorts))
    );
    return merge(serverSearch, filterFetched);
  };

  filterResorts(keyword, foundResorts): ResortResponseTo[] {
    if (!foundResorts || !keyword) {
      return [];
    }
    const regex = new RegExp(`^${keyword}`, 'i');
    const filtered = foundResorts.filter(resort => regex.test(resort.name));
    return filtered;
  }

  navigateToResortDetails($event: NgbTypeaheadSelectItemEvent) {
    if (!$event || !$event.item) {
      return false;
    }
    this.router.navigateByUrl(`/resort/${$event.item.id}`);
    return false;
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
  }
}
