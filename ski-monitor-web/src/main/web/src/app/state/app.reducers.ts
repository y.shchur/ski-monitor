import { ActionReducerMap } from '@ngrx/store';
import { AppState } from './app.state';
import { resortReducers } from '../resort/state/resort.reducers';

export const appReducers: ActionReducerMap<AppState, any> = {
  resortState: resortReducers
}