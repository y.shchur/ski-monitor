import { ResortState, initialResortState } from '../resort/state/resort.state';

export interface AppState {
  resortState: ResortState;
}

export const initialAppState = { resortState: initialResortState } as AppState;