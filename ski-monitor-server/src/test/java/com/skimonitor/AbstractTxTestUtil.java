package com.skimonitor;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

/**
 * AbstractTxTestUtil
 */
@Transactional(value = TxType.REQUIRES_NEW)
abstract public class AbstractTxTestUtil<T> {

    public abstract T save(T entity);

    public abstract void cleanDatabase();
}