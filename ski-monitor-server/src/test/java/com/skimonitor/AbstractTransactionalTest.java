package com.skimonitor;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * AbstractTransactionalTest
 */
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@ExtendWith(SpringExtension.class)
public abstract class AbstractTransactionalTest {

    @Before
    public void doSetUp() {

    }

    @After
    public void doTearDown() {

    }
}