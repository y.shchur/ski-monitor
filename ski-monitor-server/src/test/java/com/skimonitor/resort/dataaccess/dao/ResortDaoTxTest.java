package com.skimonitor.resort.dataaccess.dao;

import static org.junit.jupiter.api.Assertions.assertThrows;

import com.skimonitor.AbstractTransactionalTest;
import com.skimonitor.resort.dataaccess.domain.ResortEntity;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;

/**
 * ResortDaoComponentTest
 */
public class ResortDaoTxTest extends AbstractTransactionalTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Autowired
    private ResortDaoTxTestUtil resortTransactionalTestUtil;

    @Override
    public void doTearDown() {
        super.doTearDown();
        resortTransactionalTestUtil.cleanDatabase();
    }

    @Test
    public void shouldThrowOptimisticLockingOnSave() {
        // given
        final var resort = new ResortEntity();
        final String resortName = "Test resort";
        resort.setName(resortName);
        final var savedEntity = resortTransactionalTestUtil.save(resort);
        savedEntity.setName("Changed Name");
        final var updatedEntity = resortTransactionalTestUtil.save(savedEntity);
        updatedEntity.setVersion(0);

        // then
        assertThrows(OptimisticLockingFailureException.class, () -> {

            // when
            resortTransactionalTestUtil.save(updatedEntity);
        });
    }
}