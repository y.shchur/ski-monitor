package com.skimonitor.resort.dataaccess.dao;

import com.skimonitor.AbstractTxTestUtil;
import com.skimonitor.resort.dataaccess.domain.ResortEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ResortDaoTransactionTestUtil
 */
@Component
public class ResortDaoTxTestUtil extends AbstractTxTestUtil<ResortEntity> {

    @Autowired
    private ResortDao resortDao;

    /**
     * Saves resort entity in new transactionT
     * 
     * @param resortEntity entity to save
     * @return saved entity
     */
    @Override
    public ResortEntity save(ResortEntity resortEntity) {
        return resortDao.save(resortEntity);
    }

    /**
     * Deletes all resorts in transaction
     */
    @Override
    public void cleanDatabase() {
        resortDao.deleteAll();
    }
}