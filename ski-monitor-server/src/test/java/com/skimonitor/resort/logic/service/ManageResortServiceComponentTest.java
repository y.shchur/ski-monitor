package com.skimonitor.resort.logic.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Objects;

import com.skimonitor.AbstractComponentTest;
import com.skimonitor.resort.dataaccess.dao.ResortDao;
import com.skimonitor.resort.dataaccess.domain.ResortEntity;
import com.skimonitor.resort.logic.domain.ResortEto;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * ManageResortServiceComponentTest
 */
public class ManageResortServiceComponentTest extends AbstractComponentTest {

    @Autowired
    private ManageResortService manageResortService;

    @Autowired
    private ResortDao resortDao;

    @Test
    public void shouldSaveResort() {
        // given
        var resort = new ResortEto();
        String resortName = "Test resort";
        resort.setName(resortName);

        // when
        manageResortService.saveResort(resort);

        // then
        ResortEntity savedResort = resortDao.findAll().stream().filter(res -> Objects.equals(res.getName(), resortName))
                .findFirst().orElse(null);
        assertThat(savedResort).isNotNull();
        assertThat(savedResort.getId()).isNotNull();
        assertThat(savedResort.getVersion()).isNotNull();
    }


}