package com.skimonitor.resort.logic.service;

import com.skimonitor.AbstractComponentTest;
import com.skimonitor.resort.logic.domain.generated.WeatherResponse;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * OpenWeatherClientComponentTest
 */
public class OpenWeatherClientComponentTest extends AbstractComponentTest {

    private static final long LOCATION_ID = 707860L;

    @Autowired
    private OpenWeatherClient openWeatherClient;

    @Test
    public void shouldRetrieveWeather() {
        // given
        ReflectionTestUtils.setField(openWeatherClient, "owmApiKey", "9c0b01972933fdaa4ce89e187e81c88b");
        ReflectionTestUtils.setField(openWeatherClient, "owmWeatherUrl", "http://api.openweathermap.org/data/2.5/weather/");

        // when
        WeatherResponse response = openWeatherClient.retrieveWeatherResponse(LOCATION_ID);

        // then
        assertThat(response).isNotNull();
    }
}