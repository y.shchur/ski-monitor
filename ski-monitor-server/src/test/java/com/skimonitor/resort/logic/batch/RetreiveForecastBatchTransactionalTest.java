package com.skimonitor.resort.logic.batch;

import com.skimonitor.resort.dataaccess.dao.ResortDao;
import com.skimonitor.resort.dataaccess.dao.ResortDaoTxTestUtil;
import com.skimonitor.resort.dataaccess.domain.ResortEntity;

import org.junit.After;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * RetreiveForecastBatchTransactionalTest
 */
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class RetreiveForecastBatchTransactionalTest {

    private static final long LOCATION_ID = 707860L;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private ResortDao resortDao;

    @Autowired
    @Qualifier("forecastJob")
    private Job forecastJob;

    @Autowired
    private ResortDaoTxTestUtil resortTransactionalTestUtil;

    @After
    public void doTearDown() {
        resortTransactionalTestUtil.cleanDatabase();
    }

    @Test
    public void shouldRunBatch() throws Exception {
        var resort = new ResortEntity();
        resort.setName("TEST NAME");
        resort.setOwmId(LOCATION_ID);
        resortDao.save(resort);

        jobLauncher.run(forecastJob, new JobParameters());
    }
}