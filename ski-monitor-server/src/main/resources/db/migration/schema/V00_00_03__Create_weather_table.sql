CREATE TABLE weather (
    id                      bigint                      NOT NULL        DEFAULT nextval('hibernate_sequence'),
    version                 integer                                     DEFAULT 0,
    resort_id               bigint,
    timestamp               timestamp,
    temp                    numeric(5, 2)               NOT NULL,
    temp_min                numeric(5, 2),
    temp_max                numeric(5, 2),
    presure                 numeric(6, 2),
    sea_level               numeric(6, 2),
    grnd_level              numeric(6, 2),
    humidity                numeric(5, 2),
    rain_vol_1h             numeric(7, 2),
    rain_vol_3h             numeric(7, 2),
    snow_vol_1h             numeric(7, 2),
    snow_vol_3h             numeric(7, 2),
    wind_speed              numeric(5, 2),
    wind_dir                numeric(3, 0),
    cloudiness              numeric(3, 0),
    
    unique(resort_id, timestamp)
);