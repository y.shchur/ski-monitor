CREATE TABLE resort (
    id                      bigint                     NOT NULL        DEFAULT nextval('hibernate_sequence'),
    version                 integer                                    DEFAULT 0,
    resort_name             varchar(45)                NOT NULL,
    ski_run_total_length    numeric(6, 2)
);