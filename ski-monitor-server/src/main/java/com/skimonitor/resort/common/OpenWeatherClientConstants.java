package com.skimonitor.resort.common;

/**
 * OpenWeatherClientConstants
 */
public interface OpenWeatherClientConstants {

    String OWM_WEATHER_URL = "${server.api.owm.weather.url}";

    String OWM_FORECAST_URL = "${server.api.owm.forecast.url}";

    String OWM_API_KEY = "${server.api.owm.key}";
}