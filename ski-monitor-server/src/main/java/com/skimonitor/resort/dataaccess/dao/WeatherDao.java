package com.skimonitor.resort.dataaccess.dao;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import com.skimonitor.resort.dataaccess.domain.WeatherEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * WeatherDao
 */
public interface WeatherDao extends JpaRepository<WeatherEntity, Long> {

  @Query("SELECT weather FROM WeatherEntity weather"
      + " WHERE weather.resort.id = :resortId"
      + " AND weather.timestamp >= :dateTime"
      + " ORDER BY weather.timestamp")
  List<WeatherEntity> findLatestWeatherForecastsByResortId(@Param("resortId") Long resortId,
                                                           @Param("dateTime") Instant dateTime);

  @Query("SELECT weather FROM WeatherEntity weather"
      + " WHERE weather.resort.id IN :resortIds"
      + " AND weather.timestamp >= :fromDateTime"
      + " AND weather.timestamp <= :toDateTime"
      + " ORDER BY weather.timestamp")
  List<WeatherEntity> findForecastsByResortIdsForTimeRange(@Param("resortIds") Set<Long> resortIds,
                                                           @Param("fromDateTime") Instant fromDateTime,
                                                           @Param("toDateTime") Instant toDateTime);
}