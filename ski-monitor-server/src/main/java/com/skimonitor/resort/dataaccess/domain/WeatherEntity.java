package com.skimonitor.resort.dataaccess.domain;

import java.math.BigDecimal;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.skimonitor.common.dataaccess.domain.AbstractEntity;

import lombok.Data;

/**
 * WeatherEntity
 */
@Data
@Entity
@Table(name = "weather")
public class WeatherEntity extends AbstractEntity {

    private static final long serialVersionUID = -1114176433703044440L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "resort_id")
    private ResortEntity resort;
    /**
     * Forecast for date and time
     */
    @Column(name = "timestamp")
    private Instant timestamp;

    @Column(name = "temp", precision = 5, scale = 2)
    private BigDecimal temp;

    @Column(name = "temp_min", precision = 5, scale = 2)
    private BigDecimal tempMin;

    @Column(name = "temp_max", precision = 5, scale = 2)
    private BigDecimal tempMax;

    @Column(name = "pressure", precision = 6, scale = 2)
    private BigDecimal pressure;

    @Column(name = "sea_level", precision = 6, scale = 2)
    private BigDecimal seaLevel;

    @Column(name = "grnd_level", precision = 6, scale = 2)
    private BigDecimal grndLevel;

    @Column(name = "humidity", precision = 5, scale = 2)
    private BigDecimal humidity;

    /**
     * Rain volume for last 1 hour [mm]
     */
    @Column(name = "rain_vol_1h", precision = 7, scale = 2)
    private BigDecimal rainVol1h;

    /**
     * Rain volume for last 3 hours [mm]
     */
    @Column(name = "rain_vol_3h", precision = 7, scale = 2)
    private BigDecimal rainVol3h;

    /**
     * Snow volume for last 1 hour [mm]
     */
    @Column(name = "snow_vol_1h", precision = 7, scale = 2)
    private BigDecimal snowVol1h;

    /**
     * Snow volume for last 3 hours [mm]
     */
    @Column(name = "snow_vol_3h", precision = 7, scale = 2)
    private BigDecimal snowVol3h;

    /**
     * Wind speed [meter/sec]
     */
    @Column(name = "wind_speed", precision = 5, scale = 2)
    private BigDecimal windSpeed;

    /**
     * Wind direction [degrees]
     */
    @Column(name = "wind_dir", precision = 3, scale = 0)
    private Integer windDirection;

    /**
     * Cloudiness [%]
     */
    @Column(name = "cloudiness", precision = 3, scale = 0)
    private Integer cloudiness;

    @Column(name = "code", length = 3)
    private Integer code;
}