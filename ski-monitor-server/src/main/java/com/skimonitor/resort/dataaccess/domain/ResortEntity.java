package com.skimonitor.resort.dataaccess.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.skimonitor.common.dataaccess.domain.AbstractEntity;

import lombok.Data;

/**
 * ResortEntity
 */
@Data
@Entity
@Table(name = "resort")
public class ResortEntity extends AbstractEntity{

    private static final long serialVersionUID = 933544744970521843L;

    @Column(name = "owm_id")
    private Long owmId;

    @Column(name = "resort_name")
    private String name;

    @Column(name = "ski_run_total_length")
    private BigDecimal skiRunTotalLength;
}