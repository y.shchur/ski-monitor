package com.skimonitor.resort.dataaccess.dao;

import java.util.Collection;
import java.util.Set;

import com.skimonitor.resort.dataaccess.domain.ResortEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * ResortDao
 */
public interface ResortDao extends JpaRepository<ResortEntity, Long> {

  /**
   * Finds resorts by name match to SQL pattern
   * @param partialNamePattern name SQL pattern
   * @return found resorts
   */
  @Query("SELECT resort FROM ResortEntity resort"
      + " WHERE LOWER(resort.name) LIKE LOWER(:namePattern)")
  Collection<ResortEntity> findResortsByPartialNamePattern(@Param("namePattern") String partialNamePattern);

  @Query("SELECT resort FROM ResortEntity resort"
      + " WHERE resort.id IN :resortIds")
  Collection<ResortEntity> findResortsByIds(@Param("resortIds") Set<Long> resortIds);
}