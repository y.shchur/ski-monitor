package com.skimonitor.resort.logic.batch.forecast;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.skimonitor.resort.dataaccess.dao.WeatherDao;
import com.skimonitor.resort.dataaccess.domain.ResortEntity;
import com.skimonitor.resort.dataaccess.domain.WeatherEntity;
import com.skimonitor.resort.logic.domain.generated.WeatherResponse;
import com.skimonitor.resort.logic.mapper.WeatherMapper;
import com.skimonitor.resort.logic.service.OpenWeatherClient;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

/**
 * ForecastProcessor
 */
@Component
@RequiredArgsConstructor
public class ForecastProcessor implements ItemProcessor<ResortEntity, Collection<WeatherEntity>> {

    private final WeatherMapper weatherMapper;

    private final OpenWeatherClient openWeatherClient;

    private final WeatherDao weatherDao;

    @Override
    public Collection<WeatherEntity> process(ResortEntity resort) {
        var forecast = openWeatherClient.retrieveForecastResponse(resort.getOwmId());
        var existingWeatherForecast = weatherDao.findLatestWeatherForecastsByResortId(resort.getId(), Instant.now().atOffset(ZoneOffset.UTC).toInstant());

        Map<Instant, WeatherEntity> existingForecastMap = existingWeatherForecast.stream()
                .collect(Collectors.toMap(WeatherEntity::getTimestamp, Function.identity()));

        return forecast.getList().stream()
                .map(newWeatherData -> mapWeatherDataToWeatherEntity(newWeatherData, existingForecastMap.get(newWeatherData.getDt()), resort))
                .collect(Collectors.toList());
    }

    private WeatherEntity mapWeatherDataToWeatherEntity(WeatherResponse newWeatherData, WeatherEntity existingWeather, ResortEntity resort) {
        if (existingWeather != null) {
            weatherMapper.mapWeatherResponse2WeatherEntity(newWeatherData, existingWeather);
            return existingWeather;
        }
        var newWeather = weatherMapper.mapWeatherResponse2WeatherEntity(newWeatherData);
        newWeather.setResort(resort);
        return newWeather;
    }
}