package com.skimonitor.resort.logic.service.forecast;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import com.skimonitor.resort.logic.domain.ForecastType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ForecastSearchStrategyFactory {

  private final Map<ForecastType, ForecastSearchStrategy> forecastStrategies = new ConcurrentHashMap<>();

  @Autowired
  public ForecastSearchStrategyFactory(List<ForecastSearchStrategy> forecastStrategies) {
    forecastStrategies
        .forEach(forecastStrategy -> this.forecastStrategies.put(forecastStrategy.applicableFor(), forecastStrategy));
  }

  public ForecastSearchStrategy getStrategy(ForecastType forecastType) {
    Objects.requireNonNull(forecastType, "Forecast type should not be null");

    var strategy = forecastStrategies.get(forecastType);
    if (strategy == null) {
      throw new IllegalStateException("No strategy for forecast type");
    }
    return strategy;
  }

}