package com.skimonitor.resort.logic.domain;

import java.math.BigDecimal;
import java.time.Instant;

import com.skimonitor.common.logic.domain.AbstractTo;

import lombok.Data;

@Data
public class WeatherEto extends AbstractTo {
  
  private static final long serialVersionUID = 1L;

  private Long resortId;

  private Instant timestamp;

  private BigDecimal temp;

  private BigDecimal tempMin;

  private BigDecimal tempMax;

  private BigDecimal pressure;

  private BigDecimal seaLevel;

  private BigDecimal grndLevel;

  private BigDecimal humidity;

  private BigDecimal rainVol1h;

  private BigDecimal rainVol3h;

  private BigDecimal snowVol1h;

  private BigDecimal snowVol3h;

  private BigDecimal windSpeed;

  private Integer windDirection;

  private Integer cloudiness;

  private Integer code;
}
