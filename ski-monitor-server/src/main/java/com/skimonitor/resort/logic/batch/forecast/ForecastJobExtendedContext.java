package com.skimonitor.resort.logic.batch.forecast;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import com.skimonitor.resort.dataaccess.domain.ResortEntity;

import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * ForecastJobExtendedContext
 */
@JobScope
@Component
@Getter
@Setter
@Slf4j
public class ForecastJobExtendedContext {

  private List<ResortEntity> resorts = new ArrayList<>();

  @PostConstruct
  public void postConstruct() {
    log.debug("Forecast job extended context was created");
  }

  @PreDestroy
  public void preDestroy() {
    log.debug("Destroing forecast job extended context");
  }
}