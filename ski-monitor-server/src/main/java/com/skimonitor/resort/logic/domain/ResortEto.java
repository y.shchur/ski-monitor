package com.skimonitor.resort.logic.domain;

import java.math.BigDecimal;

import com.skimonitor.common.logic.domain.AbstractTo;

import lombok.Data;

/**
 * ResortEto
 */
@Data
public class ResortEto extends AbstractTo {

    private static final long serialVersionUID = 3898628446269320866L;

    private Long id;

    private String name;

    private BigDecimal skiRunTotalLength;
}