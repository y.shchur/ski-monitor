package com.skimonitor.resort.logic.batch.forecast;

import com.skimonitor.resort.dataaccess.dao.ResortDao;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

/**
 * Loads resorts data from database into {@link ForecastJobExtendedContext}
 */
@Component
@RequiredArgsConstructor
public class ResortReadTasklet implements Tasklet, StepExecutionListener {

    private final ResortDao resortDao;

    private final ForecastJobExtendedContext forecastJobExtendedContext;

    @Override
    public void beforeStep(StepExecution stepExecution) {
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        var resorts = resortDao.findAll();
        forecastJobExtendedContext.setResorts(resorts);
        return RepeatStatus.FINISHED;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }
}