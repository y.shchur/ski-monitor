package com.skimonitor.resort.logic.service;

import com.skimonitor.resort.logic.domain.generated.ForecastResponse;
import com.skimonitor.resort.logic.domain.generated.WeatherResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.RequiredArgsConstructor;
import com.skimonitor.resort.common.OpenWeatherClientConstants;

/**
 * OpenWeatherClient
 */
@Component
@RequiredArgsConstructor
public class OpenWeatherClient {

    @Value(OpenWeatherClientConstants.OWM_API_KEY)
    private String owmApiKey;

    @Value(OpenWeatherClientConstants.OWM_WEATHER_URL)
    private String owmWeatherUrl;

    @Value(OpenWeatherClientConstants.OWM_FORECAST_URL)
    private String owmForecastUrl;

    private final RestTemplate restTemplate;

    public WeatherResponse retrieveWeatherResponse(Long locationId) {
        var uriComponentBuilder = UriComponentsBuilder.fromHttpUrl(owmWeatherUrl)//
                .queryParam("id", locationId)//
                .queryParam("APPID", owmApiKey)//
                .queryParam("units", "metric").build();
        return restTemplate.getForObject(uriComponentBuilder.toUri(), WeatherResponse.class);
    }

    public ForecastResponse retrieveForecastResponse(Long locationId) {
        var uriComponentBuilder = UriComponentsBuilder.fromHttpUrl(owmForecastUrl)//
                .queryParam("id", locationId)//
                .queryParam("APPID", owmApiKey)//
                .queryParam("units", "metric").build();
        return restTemplate.getForObject(uriComponentBuilder.toUri(), ForecastResponse.class);
    }
}