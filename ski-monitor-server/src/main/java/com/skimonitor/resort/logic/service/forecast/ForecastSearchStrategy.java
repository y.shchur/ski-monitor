package com.skimonitor.resort.logic.service.forecast;

import java.util.List;
import java.util.Set;

import com.skimonitor.resort.logic.domain.ForecastType;
import com.skimonitor.resort.logic.domain.WeatherEto;

public interface ForecastSearchStrategy {
  
  ForecastType applicableFor();

  List<WeatherEto> findForecastForResorts(Set<Long> resortId);

}