package com.skimonitor.resort.logic.domain;

/**
 * ForecastType
 */
public enum ForecastType {

  DAILY_FORECAST,

  _5_DAY_FORECAST;

}