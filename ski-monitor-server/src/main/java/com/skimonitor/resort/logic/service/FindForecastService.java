package com.skimonitor.resort.logic.service;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.google.common.collect.Sets;
import com.skimonitor.resort.dataaccess.dao.WeatherDao;
import com.skimonitor.resort.logic.domain.ForecastType;
import com.skimonitor.resort.logic.domain.WeatherEto;
import com.skimonitor.resort.logic.mapper.WeatherMapper;
import com.skimonitor.resort.logic.service.forecast.ForecastSearchStrategyFactory;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

/**
 * Service for retrieving weather forecast data
 */
@Service
@Transactional
@RequiredArgsConstructor
public class FindForecastService {

    private final WeatherDao weatherDao;

    private final WeatherMapper weatherMapper;

    private final ForecastSearchStrategyFactory forecastSearchStrategyFactory;

    /**
     * Finds all available for resort wheather data within the time range from
     * now-1h to now+23h;
     * 
     * @param resortId id of resort
     * @return ordered by timestamp list of weather data
     */
    public List<WeatherEto> findForecastForResort(Long resortId, ForecastType forecastType) {

        return forecastSearchStrategyFactory
                .getStrategy(forecastType)
                .findForecastForResorts(Sets.newHashSet(resortId));
    }

    public Map<Long, List<WeatherEto>> findForecastsForResorts(Set<Long> resortIds, ForecastType forecastType) {
        var forecasts = forecastSearchStrategyFactory
                .getStrategy(forecastType)
                .findForecastForResorts(resortIds);
        return forecasts.stream().collect(Collectors.groupingBy(WeatherEto::getResortId));
    }

}