package com.skimonitor.resort.logic.batch.forecast;

import java.util.Iterator;

import com.skimonitor.resort.dataaccess.domain.ResortEntity;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

/**
 * Reads resorts {@link ResortEntity} from {@link ForecastJobExtendedContext}
 */
@Component
@RequiredArgsConstructor
public class ResortJobContextReader implements ItemReader<ResortEntity>, StepExecutionListener {

  private final ForecastJobExtendedContext forecastJobExtendedContext;

  private Iterator<ResortEntity> resortsIterator;

  @Override
  public void beforeStep(StepExecution stepExecution) {
    resortsIterator = forecastJobExtendedContext.getResorts().iterator();
  }

  @Override
  public ResortEntity read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
    if(resortsIterator.hasNext()){
      return resortsIterator.next();
    }
    return null;
  }

  @Override
  public ExitStatus afterStep(StepExecution stepExecution) {
    return ExitStatus.COMPLETED;
  }

}