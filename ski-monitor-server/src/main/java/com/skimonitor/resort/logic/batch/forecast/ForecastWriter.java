package com.skimonitor.resort.logic.batch.forecast;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.skimonitor.resort.dataaccess.dao.WeatherDao;
import com.skimonitor.resort.dataaccess.domain.WeatherEntity;

import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

/**
 * ForecastWriter
 */
@Component
@RequiredArgsConstructor
public class ForecastWriter implements ItemWriter<Collection<WeatherEntity>> {

    private final WeatherDao weatherDao;

    @Override
    public void write(List<? extends Collection<WeatherEntity>> items) throws Exception {
        var weatherData = items.stream().flatMap(Collection::stream).collect(Collectors.toList());
        weatherDao.saveAll(weatherData);
    }
}