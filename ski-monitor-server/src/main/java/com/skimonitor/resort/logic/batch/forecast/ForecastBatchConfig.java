package com.skimonitor.resort.logic.batch.forecast;

import java.util.Collection;

import com.skimonitor.common.ProfileConstants;
import com.skimonitor.common.config.batch.GeneralJobExecutionListener;
import com.skimonitor.resort.dataaccess.domain.ResortEntity;
import com.skimonitor.resort.dataaccess.domain.WeatherEntity;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import lombok.RequiredArgsConstructor;

/**
 * Forecast Batch Configuration
 */
@Profile(ProfileConstants.BATCH)
@Configuration
@RequiredArgsConstructor
public class ForecastBatchConfig {

  private final JobBuilderFactory jobBuilderFactory;

  private final StepBuilderFactory stepBuilderFactory;

  private final ResortReadTasklet readResortTasklet;

  private final ResortJobContextReader resortJobContextReader;

  private final ForecastProcessor forecastProcessor;

  private final ForecastWriter forecastWriter;

  private final GeneralJobExecutionListener generalJobListener;

  /**
   * Forecast batch job configuration
   * 
   * @return configured job
   */
  @Bean
  public Job forecastJob() {
    return jobBuilderFactory.get("forecastJob")//
        .listener(generalJobListener)
        .start(putAllResortsToJobContext())
        .next(retrieveForecastForResorts())//
        .build();
  }

  /**
   * Put resorts into Job context configuration
   * 
   * @return configured step
   */
  @Bean
  public Step putAllResortsToJobContext() {
    return stepBuilderFactory.get("putAllResortsToJobContext")
        .tasklet(readResortTasklet)
        .build();
  }

  /**
   * Retrieve forecast step configuration
   * 
   * @return configured step
   */
  @Bean
  public Step retrieveForecastForResorts() {
    return stepBuilderFactory.get("retrieveForecastForResorts")//
        .<ResortEntity, Collection<WeatherEntity>>chunk(10)//
        .reader(resortJobContextReader)//
        .processor(forecastProcessor)//
        .writer(forecastWriter).build();
  }
}