package com.skimonitor.resort.logic.service;

import javax.annotation.Nonnull;
import javax.transaction.Transactional;

import com.skimonitor.resort.dataaccess.dao.ResortDao;
import com.skimonitor.resort.logic.domain.ResortEto;
import com.skimonitor.resort.logic.mapper.ResortMapper;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

/**
 * ManageResortService
 */
@Service
@Transactional
@RequiredArgsConstructor
public class ManageResortService {

    private final ResortMapper resortMapper;

    private final ResortDao resortDao;

    /**
     * Saves or updates resort.
     * 
     * @param resort
     */
    public void saveResort(@Nonnull ResortEto resort) {
        if (resort.getId() == null) {
            resortDao.save(resortMapper.mapResortEto2ResortEntity(resort));
            return;
        }
        var existingResort = resortDao.getOne(resort.getId());
        resortMapper.mapResortEto2ResortEntity(resort, existingResort);
        resortDao.save(existingResort);
    }
}