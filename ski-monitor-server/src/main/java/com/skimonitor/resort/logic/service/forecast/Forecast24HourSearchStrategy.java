package com.skimonitor.resort.logic.service.forecast;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

import com.skimonitor.resort.dataaccess.dao.WeatherDao;
import com.skimonitor.resort.logic.domain.ForecastType;
import com.skimonitor.resort.logic.domain.WeatherEto;
import com.skimonitor.resort.logic.mapper.WeatherMapper;

import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class Forecast24HourSearchStrategy implements ForecastSearchStrategy {

  private final WeatherDao weatherDao;

  private final WeatherMapper weatherMapper;

  @Override
  public ForecastType applicableFor() {
    return ForecastType.DAILY_FORECAST;
  }

  @Override
  public List<WeatherEto> findForecastForResorts(Set<Long> resortIds) {
    var fromDateTime = Instant.now().atOffset(ZoneOffset.UTC).toInstant();
    var toDateTime = fromDateTime.plus(24, ChronoUnit.HOURS);
    var forecast = weatherDao.findForecastsByResortIdsForTimeRange(resortIds, fromDateTime, toDateTime);

    return weatherMapper.mapWeatherEntities2WeatherEtos(forecast);
  }

}