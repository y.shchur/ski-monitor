package com.skimonitor.resort.logic.domain;

import java.util.Set;

import lombok.Value;

@Value
public class ResortWithForecastSearchTo {
  private final Set<Long> resortIds;
  private final ForecastType forecastType;
}