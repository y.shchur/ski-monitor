package com.skimonitor.resort.logic.domain;

import java.util.List;

import com.skimonitor.common.logic.domain.AbstractTo;

import lombok.Data;

@Data
public class ResortWithForecastTo extends AbstractTo {

  private static final long serialVersionUID = 1L;

  private final ResortEto resort;
  
  private final List<WeatherEto> forecast;
}