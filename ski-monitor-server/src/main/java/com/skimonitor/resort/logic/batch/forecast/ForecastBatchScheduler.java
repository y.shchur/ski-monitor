package com.skimonitor.resort.logic.batch.forecast;

import java.util.Date;

import com.skimonitor.common.ProfileConstants;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

/**
 * Forecast Batch Scheduler
 */
@Profile(ProfileConstants.BATCH)
@Component
@RequiredArgsConstructor
public class ForecastBatchScheduler {

  @Qualifier("forecastJob")
  private final Job forecastJob;

  private final JobLauncher jobLauncher;

  /**
   * Scheduler that runs Forecast batch job
   * 
   * @throws JobExecutionAlreadyRunningException
   * @throws JobRestartException
   * @throws JobInstanceAlreadyCompleteException
   * @throws JobParametersInvalidException
   */
  @Scheduled(cron = "${batch.forecastJob.cron}")
  public void schedule() throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
    jobLauncher.run(forecastJob, new JobParametersBuilder()
        .addDate("date", new Date())
        .toJobParameters());
  }
}