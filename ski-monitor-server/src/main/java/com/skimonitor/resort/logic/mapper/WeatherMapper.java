package com.skimonitor.resort.logic.mapper;

import java.util.Collection;
import java.util.List;

import com.skimonitor.resort.dataaccess.domain.WeatherEntity;
import com.skimonitor.resort.logic.domain.WeatherEto;
import com.skimonitor.resort.logic.domain.generated.WeatherResponse;

import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

/**
 * WeatherMapper
 */
@Mapper(componentModel = "spring")
public interface WeatherMapper {

    /**
     * Map {@link WeatherResponse} to {@link WeatherEntity}
     * 
     * @param source weather response
     * @return weather entity
     */
    @Mappings({ @Mapping(source = "dt", target = "timestamp"), @Mapping(source = "main.temp", target = "temp"),
            @Mapping(source = "main.tempMin", target = "tempMin"),
            @Mapping(source = "main.tempMax", target = "tempMax"),
            @Mapping(source = "main.pressure", target = "pressure"),
            @Mapping(source = "main.seaLevel", target = "seaLevel"),
            @Mapping(source = "main.grndLevel", target = "grndLevel"),
            @Mapping(source = "main.humidity", target = "humidity"),
            @Mapping(source = "rain.vol1h", target = "rainVol1h"),
            @Mapping(source = "rain.vol3h", target = "rainVol3h"),
            @Mapping(source = "snow.vol1h", target = "snowVol1h"),
            @Mapping(source = "snow.vol3h", target = "snowVol3h"),
            @Mapping(source = "wind.speed", target = "windSpeed"),
            @Mapping(source = "wind.deg", target = "windDirection"),
            @Mapping(source = "clouds.all", target = "cloudiness"),
            @Mapping(expression = "java(source.getWeather().get(0).getId())", target = "code") })
    WeatherEntity mapWeatherResponse2WeatherEntity(WeatherResponse source);

    @InheritConfiguration
    void mapWeatherResponse2WeatherEntity(WeatherResponse source, @MappingTarget WeatherEntity target);

    /**
     * Map collection of {@link WeatherResponse} to collection {@link WeatherEntity}
     * 
     * @param source weather response collection
     * @return weather entity collection
     */
    Collection<WeatherEntity> mapWeatherResponse2WeatherEntity(Collection<WeatherResponse> source);

    /**
     * Map {@link WeatherEntity} to {@link WeatherEto}
     * 
     * @param source weather entity
     * @return weather eto
     */
    @Mappings(@Mapping(source = "source.resort.id", target = "resortId"))
    WeatherEto mapWeatherEntity2WeatherEto(WeatherEntity source);

    /**
     * Map list of {@link WeatherEntity} to a list of {@link WeatherEto}
     * 
     * @param source list of weather entities
     * @return weather eto list
     */
    List<WeatherEto> mapWeatherEntities2WeatherEtos(List<WeatherEntity> source);
}