package com.skimonitor.resort.logic.service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import com.skimonitor.resort.dataaccess.dao.ResortDao;
import com.skimonitor.resort.dataaccess.domain.ResortEntity;
import com.skimonitor.resort.logic.domain.ResortWithForecastTo;
import com.skimonitor.resort.logic.domain.WeatherEto;
import com.skimonitor.resort.logic.domain.ForecastType;
import com.skimonitor.resort.logic.domain.ResortEto;
import com.skimonitor.resort.logic.domain.ResortWithForecastSearchTo;
import com.skimonitor.resort.logic.mapper.ResortMapper;
import com.skimonitor.resort.logic.service.forecast.ForecastSearchStrategy;
import com.skimonitor.resort.logic.service.forecast.ForecastSearchStrategyFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lombok.RequiredArgsConstructor;

/**
 * FindResortService
 */
@Service
@Transactional
@RequiredArgsConstructor
public class FindResortService {

    private final FindForecastService findForecastService;

    private final ResortMapper resortMapper;

    private final ResortDao resortDao;

    /**
     * Finds all resorts
     * 
     * @return resorts
     */
    public Collection<ResortEto> findAllResorts() {
        return resortMapper.mapResortEntities2ResortEtos(resortDao.findAll());
    }

    /**
     * Finds resorts by partial name match
     * 
     * @param name to match
     * @return resorts
     */
    public Collection<ResortEto> findResortsMatchingName(String name) {
        var namePattern = name + "%";
        var foundResorts = resortDao.findResortsByPartialNamePattern(namePattern);
        return resortMapper.mapResortEntities2ResortEtos(foundResorts);
    }

    public ResortWithForecastTo findResortWithForecast(Long resortId, ForecastType forecastType) {
        var resort = resortMapper.mapResortEntity2ResortEto(resortDao.getOne(resortId));
        var forecast = findForecastService.findForecastForResort(resortId, forecastType);

        return new ResortWithForecastTo(resort, forecast);
    }

    public Collection<ResortWithForecastTo> findForecastsByResortIdsAndType(ResortWithForecastSearchTo searchTo) {
        if (searchTo == null || CollectionUtils.isEmpty(searchTo.getResortIds())) {
            return new LinkedList<>();
        }

        Collection<ResortEto> resorts = resortMapper.mapResortEntities2ResortEtos(
                resortDao.findResortsByIds(searchTo.getResortIds()));

        Map<Long, List<WeatherEto>> forecasts = findForecastService
                .findForecastsForResorts(searchTo.getResortIds(), searchTo.getForecastType());

        Collection<ResortWithForecastTo> resortsWithForecasts = new LinkedList<>();
        for (var resort : resorts) {
            resortsWithForecasts.add(new ResortWithForecastTo(resort, forecasts.get(resort.getId())));
        }
        return resortsWithForecasts;
    }
}