package com.skimonitor.resort.logic.mapper;

import java.util.Collection;

import com.skimonitor.resort.dataaccess.domain.ResortEntity;
import com.skimonitor.resort.logic.domain.ResortEto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

/**
 * ResortMapper
 */
@Mapper(componentModel = "spring")
public interface ResortMapper {
    /**
     * Map resort entity to eto
     * 
     * @param source
     * @return mapped eto
     */
    ResortEto mapResortEntity2ResortEto(ResortEntity source);

    /**
     * Map collection of entities to collection of etos
     * 
     * @param source
     * @return mapped etos
     */
    Collection<ResortEto> mapResortEntities2ResortEtos(Collection<ResortEntity> source);

    /**
     * Map eto to entity
     * 
     * @param source
     * @return
     */
    @Mappings({ @Mapping(target = "version", ignore = true) })
    ResortEntity mapResortEto2ResortEntity(ResortEto source);

    /**
     * Map eto to entity
     * 
     * @param source
     * @param target
     */
    @Mappings({ @Mapping(target = "id", ignore = true), //
            @Mapping(target = "version", ignore = true) })
    void mapResortEto2ResortEntity(ResortEto source, @MappingTarget ResortEntity target);
}