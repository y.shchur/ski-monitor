package com.skimonitor.resort;

import java.util.Collection;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skimonitor.resort.logic.domain.ForecastType;
import com.skimonitor.resort.logic.domain.ResortEto;
import com.skimonitor.resort.logic.domain.ResortWithForecastSearchTo;
import com.skimonitor.resort.logic.domain.ResortWithForecastTo;
import com.skimonitor.resort.logic.service.FindResortService;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * ResortRestController
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class ResortRestController {

    private final FindResortService findResortService;

    @GetMapping(ResortRestConstants.GET_ALL_RESORTS)
    public Collection<ResortEto> findAllResorts() {
        return findResortService.findAllResorts();
    }

    @GetMapping("/api/resort/findMatch/{name}")
    public Collection<ResortEto> findMatching(@PathVariable String name) {
        log.debug("Search for resorts matching to: " + name);
        return findResortService.findResortsMatchingName(name);
    }

    @GetMapping("api/resort/findForecastByResortId")
    public ResortWithForecastTo findForecastByResortId(@RequestParam("id") Long resortId,
                                                       @RequestParam("forecastType") ForecastType forecastType) {
        log.debug("Search for forecast by resort id: {} and forecast type: {}", resortId, forecastType);
        return findResortService.findResortWithForecast(resortId, forecastType);
    }

    @GetMapping("api/resort/findForecastsByResortIdsAndType")
    public Collection<ResortWithForecastTo> findForecastsByResortIdsAndType(@RequestParam("resortIds") Set<Long> resortIds,
                                                                            @RequestParam("forecastType") ForecastType forecastType) {
        var searchTo = new ResortWithForecastSearchTo(resortIds, forecastType);
        log.debug("Search for forecasts by resort ids: {} and forecast type: {}", searchTo.getResortIds(), searchTo.getForecastType());
        return findResortService.findForecastsByResortIdsAndType(searchTo);
    }
}