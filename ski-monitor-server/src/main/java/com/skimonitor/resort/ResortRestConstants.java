package com.skimonitor.resort;

/**
 * ResortRestConstants
 */
public interface ResortRestConstants {

    static final String BASE_URL = "/api/resort";

    static final String GET_ALL_RESORTS = BASE_URL + "/all";

    static final String GET_MATCHING_RESORTS = BASE_URL + "/findMatch/{name}";
}