package com.skimonitor.common.config.batch;

import java.text.MessageFormat;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

import lombok.extern.java.Log;

/**
 * GeneralJobExecutionListener
 */
@Component
@Log
public class GeneralJobExecutionListener implements JobExecutionListener {

    private static final String JOB_STARTED_MSG = "Batch job {0} with id {1} started";
    private static final String JOB_ENDED_MSG = "Batch job {0} with id {1} ended with exit status {2}";

    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.info(MessageFormat.format(JOB_STARTED_MSG, jobExecution.getJobConfigurationName(), jobExecution.getJobId()));
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info(MessageFormat.format(JOB_ENDED_MSG, jobExecution.getJobConfigurationName(), jobExecution.getJobId(), jobExecution.getExitStatus()));
    }

}