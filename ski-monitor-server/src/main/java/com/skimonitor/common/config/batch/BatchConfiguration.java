package com.skimonitor.common.config.batch;

import com.skimonitor.common.ProfileConstants;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * BatchConfiguration
 */
@Profile(ProfileConstants.BATCH)
@Configuration
@EnableBatchProcessing  
@EnableScheduling
public class BatchConfiguration {
    
   
}