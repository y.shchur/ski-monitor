package com.skimonitor.common.dataaccess.domain;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import lombok.Data;

/**
 * AbstractEntity
 */
@Data
@MappedSuperclass
public abstract class AbstractEntity implements Serializable{

    private static final long serialVersionUID = 8058906151094570312L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @Version
    private Integer version;
}