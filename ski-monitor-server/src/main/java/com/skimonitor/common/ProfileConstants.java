package com.skimonitor.common;

/**
 * ProfileConstants
 */
public interface ProfileConstants {

  /**
   * Batch profile constant
   */
  String BATCH = "batch";

}